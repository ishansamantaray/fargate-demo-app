# The AWS registry now has our new container, but our cluster isn't aware that a new version
# of the container is available. We need to create an updated task definition. Task definitions
# always have a version number. When we register a task definition using a name that already
# exists, AWS automatically increments the previously used version number for the task
# definition with that same name and uses it here. Note that we also define CPU and memory
# requirements here and give it a JSON file describing our task definition that I've saved
# to my repository in a aws/ directory.
aws ecs register-task-definition --family AWS-CICD-PIPELINE-TASK --requires-compatibilities FARGATE --cpu 256 --memory 512 --cli-input-json file://scripts/task-definition.json --region $AWS_REGION

# Tell our service to use the latest version of task definition.
aws ecs update-service --cluster AWS-CICD-PIPELINE-CLUSTER --service sample-app-service --task-definition  AWS-CICD-PIPELINE-TASK --region $AWS_REGION